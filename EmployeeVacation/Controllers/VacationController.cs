﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using EmployeeVacation.DataContext.Domain.Models;
using EmployeeVacation.Dto;
using EmployeeVacation.Exceptions;
using EmployeeVacation.Mapper;
using EmployeeVacation.Services.Interfaces;

namespace EmployeeVacation.Controllers
{
    public class VacationController : ApiController
    {
        private readonly IVacationService _vacationService;

        public VacationController(IVacationService vacationService)
        {
            _vacationService = vacationService;
        }

        [HttpGet]
        public List<VacationDto> GetAll()
        {
            List<Vacation> vacations = _vacationService.GetAll().OrderBy(x=>x.StartDate).ToList();
            return vacations.ConvertAll(x => x.Map());
        }

        [HttpPost]
        public ResponseDto Save(VacationDto vacationDto)
        {
            Vacation vacation = vacationDto.Map();
            try
            {
                _vacationService.Save(vacation);
            }
            catch (VacationValidationException e)
            {
                return new ResponseDto
                {
                    StatusCode = HttpStatusCode.BadRequest,
                    Message = e.Message
                };
            }
            return new ResponseDto
            {
                StatusCode = HttpStatusCode.OK
            };
        }
    }
}