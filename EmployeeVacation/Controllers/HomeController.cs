﻿using System.Web.Mvc;

namespace EmployeeVacation.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "EmployeeVacation";

            return View();
        }
    }
}
