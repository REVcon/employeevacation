﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using EmployeeVacation.DataContext.Domain.Models;
using EmployeeVacation.Dto;
using EmployeeVacation.Mapper;
using EmployeeVacation.Services.Interfaces;

namespace EmployeeVacation.Controllers
{
    public class EmployeeController : ApiController
    {
        private readonly IEmployeeService _employeeService;

        public EmployeeController(IEmployeeService employeeService)
        {
            _employeeService = employeeService;
        }

        [HttpGet]
        public List<EmployeeDto> GetAll()
        {
            List<Employee> employees = _employeeService.GetAll();
            return employees.ConvertAll(x => x.Map());
        }


        [HttpPost]
        public ResponseDto Save(EmployeeDto employeeDto)
        {
            if (ModelState.IsValid)
            {
                Employee employee = employeeDto.Map();
                _employeeService.Save(employee);
                return new ResponseDto
                {
                    StatusCode = HttpStatusCode.OK
                };
            }
            return new ResponseDto
            {
                StatusCode = HttpStatusCode.BadRequest,
                Message = ModelState.Values.SelectMany(x => x.Errors).First().ErrorMessage
            };
        }
    }
}