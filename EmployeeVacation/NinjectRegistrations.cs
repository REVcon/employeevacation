﻿using System.Data.Entity;
using EmployeeVacation.DataContext;
using EmployeeVacation.Repository;
using EmployeeVacation.Repository.Interfaces;
using EmployeeVacation.Services;
using EmployeeVacation.Services.Interfaces;
using Ninject.Modules;
using Ninject.Web.Common;

namespace EmployeeVacation
{
    public class NinjectRegistrations : NinjectModule
    {
        public override void Load()
        {
            Bind<EmployeeVacationContext>().ToSelf().InRequestScope();
            Bind(typeof(IGenericRepository<>)).To(typeof(GenericRepository<>)).InRequestScope();
            Bind<IVacationService>().To<VacationService>().InRequestScope();
            Bind<IEmployeeService>().To<EmployeeService>().InRequestScope();
        }
    }
}