﻿using System;

namespace EmployeeVacation.Exceptions
{
    public class VacationValidationException : Exception
    {
        public VacationValidationException(string message) : base(message)
        {
        }
    }
}