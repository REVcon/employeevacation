﻿using System;
using System.Runtime.Serialization;

namespace EmployeeVacation.Dto
{
    [DataContract]
    public class VacationDto
    {
        [DataMember(Name = "employee")]
        public string Employee { get; set; }

        [DataMember(Name = "employeeId")]
        public int EmployeeId { get; set; }

        [DataMember(Name = "startDate")]
        public DateTime StartDate { get; set; }

        [DataMember(Name = "endDate")]
        public DateTime EndDate { get; set; }
    }
}