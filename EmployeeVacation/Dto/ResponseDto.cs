﻿using System.Net;
using System.Runtime.Serialization;

namespace EmployeeVacation.Dto
{
    [DataContract]
    public class ResponseDto
    {
        [DataMember(Name = "statusCode")]
        public HttpStatusCode StatusCode { get; set; }

        [DataMember(Name = "message")]
        public string Message { get; set; }
    }
}