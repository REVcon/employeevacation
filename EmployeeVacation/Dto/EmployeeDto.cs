﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace EmployeeVacation.Dto
{
    [DataContract]
    public class EmployeeDto
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [Required(ErrorMessage = "Имя должна быть установлено")]
        [MaxLength(50, ErrorMessage = "Имя не должно быть длиннее 50 символов")]
        [DataMember(Name = "name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Фамилия должна быть установлена")]
        [MaxLength(50,ErrorMessage = "Фамилия не должна быть длиннее 50 символов")]
        [DataMember(Name = "surname")]
        public string Surname { get; set; }
    }
}