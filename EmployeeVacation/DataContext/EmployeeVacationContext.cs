﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using EmployeeVacation.DataContext.Domain.Configuration;
using EmployeeVacation.DataContext.Domain.Models;

namespace EmployeeVacation.DataContext
{
    public class EmployeeVacationContext : DbContext
    {
        public EmployeeVacationContext() : base("employee_vacation")
        {
            Database.SetInitializer<EmployeeVacationContext>(new CreateDatabaseIfNotExists<EmployeeVacationContext>());
        }

        public DbSet<Employee> Employees { get; set; }
        public DbSet<Vacation> Vacations { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new EmployeeConfig());
            modelBuilder.Configurations.Add(new VacationConfig());
        }
    }
}