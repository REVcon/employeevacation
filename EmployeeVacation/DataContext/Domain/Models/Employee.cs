﻿using System.Collections.Generic;

namespace EmployeeVacation.DataContext.Domain.Models
{
    public class Employee
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }

        public virtual ICollection<Vacation> Vacations { get; set; }
    }
}