﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using EmployeeVacation.DataContext.Domain.Models;

namespace EmployeeVacation.DataContext.Domain.Configuration
{
    public class EmployeeConfig : EntityTypeConfiguration<Employee>
    {
        public EmployeeConfig()
        {
            ToTable("employee").HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.Name).HasColumnName("name").HasMaxLength(50).IsRequired();
            Property(x => x.Surname).HasColumnName("surname").HasMaxLength(50).IsRequired();

            HasMany(x => x.Vacations).WithRequired(x => x.Employee).HasForeignKey(x => x.EmployeeId)
                .WillCascadeOnDelete();
        }
    }
}