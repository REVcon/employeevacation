﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using EmployeeVacation.DataContext.Domain.Models;

namespace EmployeeVacation.DataContext.Domain.Configuration
{
    public class VacationConfig : EntityTypeConfiguration<Vacation>
    {
        public VacationConfig()
        {
            ToTable("vacation").HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.EmployeeId).HasColumnName("employee_id");
            Property(x => x.StartDate).HasColumnName("start_date").HasColumnType("date");
            Property(x => x.EndDate).HasColumnName("end_date").HasColumnType("date");

            HasRequired(x => x.Employee).WithMany(x => x.Vacations).HasForeignKey(x => x.EmployeeId)
                .WillCascadeOnDelete();
        }
    }
}