﻿using System.Collections.Generic;
using System.Linq;
using EmployeeVacation.DataContext.Domain.Models;
using EmployeeVacation.Repository.Interfaces;
using EmployeeVacation.Services.Interfaces;

namespace EmployeeVacation.Services
{
    internal class EmployeeService : IEmployeeService
    {
        private readonly IGenericRepository<Employee> _repository;

        public EmployeeService(IGenericRepository<Employee> repository)
        {
            _repository = repository;
        }

        public void Save(Employee employee)
        {
            _repository.Create(employee);
        }

        public List<Employee> GetAll()
        {
            return _repository.Get().ToList();
        }

        public Employee Get(int employeeId)
        {
            return _repository.FindById(employeeId);
        }
    }
}