﻿using System.Collections.Generic;
using EmployeeVacation.DataContext.Domain.Models;

namespace EmployeeVacation.Services.Interfaces
{
    public interface IEmployeeService
    {
        void Save(Employee employee);
        List<Employee> GetAll();
        Employee Get(int employeeId);
    }
}