﻿using System.Collections.Generic;
using EmployeeVacation.DataContext.Domain.Models;

namespace EmployeeVacation.Services.Interfaces
{
    public interface IVacationService
    {
        List<Vacation> GetAll();
        void Save(Vacation vacation);
    }
}