﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using EmployeeVacation.DataContext.Domain.Models;
using EmployeeVacation.Exceptions;
using EmployeeVacation.Repository.Interfaces;
using EmployeeVacation.Services.Interfaces;

namespace EmployeeVacation.Services
{
    internal class VacationService : IVacationService
    {
        private const int MaxVacationInYear = 28;

        private static readonly object Lock = new object();
        private readonly IGenericRepository<Vacation> _repository;
        private readonly IEmployeeService _employeeService;

        public VacationService(IGenericRepository<Vacation> repository, IEmployeeService employeeService)
        {
            _repository = repository;
            _employeeService = employeeService;
        }

        public List<Vacation> GetAll()
        {
            return _repository.Get().ToList();
        }

        public void Save(Vacation vacation)
        {
            Employee employee = _employeeService.Get(vacation.EmployeeId);
            if (employee == null)
            {
                throw new VacationValidationException("Сотрудник не найден");
            }

            if (vacation.EndDate < vacation.StartDate)
            {
                throw new VacationValidationException("Дата окончания отпуска не может быть меньше даты начала");
            }

            lock (Lock)
            {
                List<Vacation> employeeVacations = _repository.Get(x => x.EmployeeId == vacation.EmployeeId).ToList();
                Dictionary<int, int> currentVacation = GetVacationByYears(employeeVacations);
                Dictionary<int, int> newVacation = GetVacationByYears(new List<Vacation> {vacation});
                foreach (int key in newVacation.Keys)
                {
                    int currentVacationLenInYear = currentVacation.ContainsKey(key) ? currentVacation[key] : 0;
                    int vacationLen = currentVacationLenInYear + newVacation[key];
                    if (vacationLen > MaxVacationInYear)
                    {
                        throw new VacationValidationException($"В {key} году отпуск больше 28 дней");
                    }
                }

                Vacation intersectedVacation = CheckIntersectionWithAnotherVacation(vacation);
                if (intersectedVacation != null)
                {
                    throw new VacationValidationException(
                        $"Пересечение с отпуском сотрудника {intersectedVacation.Employee.Name} {intersectedVacation.Employee.Surname}");
                }

                _repository.Create(vacation);
            }
        }

        private Dictionary<int, int> GetVacationByYears(List<Vacation> vacations)
        {
            Dictionary<int, int> result = new Dictionary<int, int>();
            foreach (var vacation in vacations)
            {
                int year = vacation.StartDate.Year;
                int maxYear = vacation.EndDate.Year;
                DateTime vacaionPeriodInYearStart = vacation.StartDate;
                do
                {
                    var vacaionPeriodInYearEnd = year == vacation.EndDate.Year
                        ? vacation.EndDate
                        : new DateTime(year, 12, 31);
                    int periodLen = (vacaionPeriodInYearEnd.Date - vacaionPeriodInYearStart.Date).Days + 1;
                    if (result.ContainsKey(year))
                    {
                        result[year] = result[year] + periodLen;
                    }
                    else
                    {
                        result[year] = periodLen;
                    }
                    year++;
                    vacaionPeriodInYearStart = new DateTime(year, 1, 1);
                } while (maxYear >= year);
            }
            return result;
        }

        private Vacation CheckIntersectionWithAnotherVacation(Vacation vacation)
        {
            return _repository.Table.Include(x => x.Employee)
                .FirstOrDefault(x => x.StartDate <= vacation.EndDate && vacation.StartDate <= x.EndDate);
        }
    }
}