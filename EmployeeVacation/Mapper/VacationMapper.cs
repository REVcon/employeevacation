﻿using EmployeeVacation.DataContext.Domain.Models;
using EmployeeVacation.Dto;

namespace EmployeeVacation.Mapper
{
    internal static class VacationMapper
    {
        public static VacationDto Map(this Vacation vacation)
        {
            return new VacationDto
            {
                Employee = $"{vacation.Employee.Name} {vacation.Employee.Surname}",
                StartDate = vacation.StartDate,
                EndDate = vacation.EndDate
            };
        }

        public static Vacation Map(this VacationDto vacationDto)
        {
            return new Vacation
            {
                EmployeeId = vacationDto.EmployeeId,
                StartDate = vacationDto.StartDate.Date,
                EndDate = vacationDto.EndDate.Date
            };
        }
    }
}