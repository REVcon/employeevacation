﻿using EmployeeVacation.DataContext.Domain.Models;
using EmployeeVacation.Dto;

namespace EmployeeVacation.Mapper
{
    internal static class EmployeeMapper
    {
        public static Employee Map(this EmployeeDto employeeDto)
        {
            return new Employee
            {
                Name = employeeDto.Name,
                Surname = employeeDto.Surname
            };
        }

        public static EmployeeDto Map(this Employee employee)
        {
            return new EmployeeDto
            {
                Id = employee.Id,
                Name = employee.Name,
                Surname = employee.Surname
            };
        }
    }
}