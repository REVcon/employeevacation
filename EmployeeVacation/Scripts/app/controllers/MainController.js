﻿angular.module("employeeVacationApp").controller("MainController",
    [
        "$scope", "$uibModal", "VacationService", "EmployeeService", "$q",
        function($scope, $uibModal, vacationService, employeeService, $q) {
            $scope.alerts = [];

            $scope.isTableVisible = function() {
                return $scope.vacations.length > 0 && !$scope.loading;
            };

            $scope.openAddEmployeePopup = function() {
                var modalInstance = $uibModal.open({
                    animation: false,
                    backdrop: false,
                    templateUrl: "Scripts/app/directives/employeePopup/EmployeePopupTemplate.html",
                    controller: "EmployeePopupController",
                    size: "lg",
                    resolve: {
                        employee: function() {
                            return $scope.employee;
                        },
                        alert: function() {
                            return $scope.employeeAlert;
                        }
                    }
                });
                modalInstance.result.then(function(employee) {
                        $scope.employee = employee;
                        $scope.showPreloader();
                        employeeService.save($scope.employee).$promise
                            .then(function(response) {
                                $scope.hidePreloader();
                                if (response.statusCode !== 200) {
                                    $scope.employeeAlert.message = response.message;
                                    $scope.employeeAlert.isVisible = true;
                                    $scope.openAddEmployeePopup();
                                } else {
                                    init();
                                    $scope.alerts.push({
                                        type: 'success',
                                        msg: 'Сотрудник ' + employee.name + ' ' + employee.surname + ' добавлен'
                                    });
                                }
                            }).catch(function () {
                                $scope.hidePreloader();
                            });;
                    },
                    function(error) {
                    });
            };

            $scope.openAddVacationPopup = function() {
                var modalInstance = $uibModal.open({
                    animation: false,
                    backdrop: false,
                    templateUrl: "Scripts/app/directives/vacationPopup/VacationPopupTemplate.html",
                    controller: "VacationPopupController",
                    size: "lg",
                    resolve: {
                        employees: function() {
                            return $scope.employees;
                        },
                        vacation: function() {
                            return $scope.vacation;
                        },
                        alert: function() {
                            return $scope.vacationAlert;
                        }
                    }
                });
                modalInstance.result.then(function(vacation) {
                        $scope.vacation = vacation;
                        $scope.showPreloader();
                        vacationService.save($scope.vacation).$promise
                            .then(function (response) {
                                $scope.hidePreloader();
                                if (response.statusCode !== 200) {
                                    $scope.vacationAlert.message = response.message;
                                    $scope.vacationAlert.isVisible = true;
                                    $scope.openAddVacationPopup();
                                } else {
                                    init();
                                }
                            })
                            .catch(function () {
                                $scope.hidePreloader();
                            });
                    },
                    function(error) {
                    });
            };

            $scope.closeAlert = function(index) {
                $scope.alerts.splice(index, 1);
            };

            $scope.showPreloader = function() {
                $scope.loading = true;
            };

            $scope.hidePreloader = function() {
                $scope.loading = false;
            };

            function init() {
                $scope.showPreloader();

                $scope.vacation = {};
                $scope.vacation.startDate = new Date();
                $scope.vacation.endDate = new Date();
                $scope.vacationAlert = { message: "", isVisible: false };

                $scope.employee = {};
                $scope.employeeAlert = { message: "", isVisible: false };

                $scope.vacations = [];
                $q.all([vacationService.get().$promise, employeeService.get().$promise])
                    .then(function(result) {
                        $scope.vacations = result[0];
                        $scope.employees = result[1];
                        $scope.hidePreloader();
                    });
            };

            init();
        }
    ]);