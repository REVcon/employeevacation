﻿angular.module('employeeVacationApp').factory('EmployeeService',
    [
        '$resource', function ($resource) {
            return $resource('/api/employee', {}, {
                get: { method: 'GET', isArray: true },
                save: { method: 'POST' }
            });
        }
    ]);