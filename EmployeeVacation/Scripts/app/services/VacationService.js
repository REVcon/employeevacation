﻿angular.module('employeeVacationApp').factory('VacationService',
    [
        '$resource', function($resource) {
            return $resource('/api/vacation', {}, {
                get: { method: 'GET', isArray: true},
                update: { method: 'PUT' }
            });
        }
    ]);