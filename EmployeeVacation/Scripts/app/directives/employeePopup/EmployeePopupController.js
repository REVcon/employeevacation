﻿angular.module('employeeVacationApp').controller('EmployeePopupController',
    function ($scope, $uibModalInstance, employee, alert) {
        $scope.employee = employee;
        $scope.alert = alert;

        $scope.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.addEmployee = function () {
            if ($scope.employeeForm.$valid) {
                $uibModalInstance.close($scope.employee);
            } else {
                showAlert('Проверьте заполнены ли поля, максимальная длина 50 символов');
            }
        };

        function showAlert(message) {
            $scope.alert.message = message;
            $scope.alert.isVisible = true;
        }
    }
);