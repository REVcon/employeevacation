﻿angular.module("employeeVacationApp").controller("VacationPopupController",
    function ($scope, $uibModalInstance, employees, vacation, alert) {
        $scope.employees = employees;
        $scope.alert = alert;
        $scope.vacation = vacation;

        $scope.cancel = function() {
            $uibModalInstance.dismiss("cancel");
        };
        $scope.addVacation = function() {
            if ($scope.vacationForm.$valid) {
                $uibModalInstance.close($scope.vacation);
            } else {
                showAlert("Проверьте заполнены ли все поля, дата окончания отпуска не должна быть меньше даты начала");
            }
        };

        function showAlert(message) {
            $scope.alert.message = message;
            $scope.alert.isVisible = true;
        }
    }
);